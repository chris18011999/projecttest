import { Injectable } from '@angular/core';
import { tokenNotExpired } from '../../../node_modules/angular2-jwt';
import { Router } from '@angular/router';

declare const Auth0Lock: any;
@Injectable()
export class Auth{

  //configure Auth0
  lock = new Auth0Lock("TC4a3MHPLGMsYshJYV4ugSf76hMFjMfL", 'chrism.eu.auth0.com', {
    rememberLastLogin: false,
  redirectUrl: "/dashboard",
  languageDictionary: {
    title: "Log in with ByCycling"
  },
  allowSignUp: false,
  allowedConnections: ['Username-Password-Authentication'],
  theme: {
    logo: '/src/app/img/BC-favicon.png',
    primaryColor: '#16112d'
  }});
  profile: Object;
  

  constructor(){
    this.lock.on('authenticated', (authResult:any) => {
      localStorage.setItem('id_token', authResult.idToken);
      this.profile = JSON.parse(localStorage.getItem('profile'));

      this.lock.getProfile(authResult.idToken, (error: any, profile: any) => {
      if (error) {
        console.log(error);
      }

      localStorage.setItem('profile', JSON.stringify(profile));
    });
    });
  }
  public login(){
    this.lock.show();
  }
  public authenticated(){
    return tokenNotExpired();
  }
  public logout(){
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
  }
}
