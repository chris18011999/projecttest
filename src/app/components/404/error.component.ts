import {Component} from "@angular/core";
import {Auth} from "../../services/auth.service";


@Component({
  selector: 'error-page',
  template: 'I am the 404 page'
})

export class ErrorComponent {
  constructor(private auth: Auth){

  }
}
