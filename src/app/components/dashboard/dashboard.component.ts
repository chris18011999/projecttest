import {Component} from "@angular/core";
import {Auth} from "../../services/auth.service";

@Component({
  selector: 'dashboard-page',
  templateUrl: 'dashboard.component.html'
})

export class DashboardComponent {
  constructor(private auth: Auth){}
}
