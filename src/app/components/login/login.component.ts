import {Component} from "@angular/core";
import {Auth} from "../../services/auth.service";


@Component({
  selector: 'login-page',
  templateUrl: 'login.component.html'
})

export class LoginComponent {
  constructor(private auth: Auth) {

  }
}
