import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {routing, appRoutingProviders} from "./app.routing";
import {AppComponent} from "./app.component";
import {AUTH_PROVIDERS} from "../../node_modules/angular2-jwt/angular2-jwt";
//import the components/views
import {HomeComponent} from "./components/home/home.component";
import {ErrorComponent} from "./components/404/error.component";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import {SettingsComponent} from "./components/settings/settings.component";
import {ResetpassComponent} from "./components/resetpass/resetpass.component";
import {FeedbackComponent} from "./components/feedback/feedback.component";
import {Auth} from "./services/auth.service";


@NgModule({
  imports: [BrowserModule, routing],
  declarations: [
    AppComponent,
    HomeComponent,
    ErrorComponent,
    DashboardComponent,
    LoginComponent,
    RegisterComponent,
    SettingsComponent,
    ResetpassComponent,
    FeedbackComponent
  ],
  bootstrap: [AppComponent],
  providers: [appRoutingProviders, Auth]

})

export class AppModule {}
