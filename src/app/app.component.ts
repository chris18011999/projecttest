import { Component } from "@angular/core";
import { Auth } from './services/auth.service';

@Component({
  selector: 'my-app',
  styleUrls: ["css/style.css"],
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(private auth: Auth){

  }
}
