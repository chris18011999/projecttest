import { ProjecttestPage } from './app.po';

describe('projecttest App', () => {
  let page: ProjecttestPage;

  beforeEach(() => {
    page = new ProjecttestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
